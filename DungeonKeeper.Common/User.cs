﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DungeonKeeper.Common
{
    public class User
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9]*$", ErrorMessage = "Username must be alphanumeric characters only.")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "You must specify an username between 3 and 20 characters.")]
        public string Username { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "You must specify a password between 3 and 20 characters.")]
        public string Password { get; set; }
    }

    public class RegisterUser : User
    {
        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email address must be valid.")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "You must specify an email between 3 and 20 characters.")]
        public string Email { get; set; }
    }
}
