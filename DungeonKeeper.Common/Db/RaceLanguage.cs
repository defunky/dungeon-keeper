﻿namespace DungeonKeeper.Common.Db
{
    public class RaceLanguage
    {
        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public int RaceId { get; set; }
        public Race Race { get; set; }
    }
}
