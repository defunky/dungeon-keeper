﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DungeonKeeper.Common.Db
{
    public class Race
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<RaceAbilityScore> RaceAbilityScores { get; set; }

        public string Age { get; set; }

        public string Alignment { get; set; }
        public Sizes Size { get; set; }
        public string SizeDescription { get; set; }

        public int Speed { get; set; }
        public string SpeedDescription { get; set; }

        public IList<RaceLanguage> RaceLanguages { get; set; }
        public string LanguageDescription { get; set; }
        public IList<RaceTrait> RaceTraits { get; set; }

        public IList<Subrace.Subrace> SubRaces { get; set; }
        public SourceMaterial SourceMaterial { get; set; }
    }
}
