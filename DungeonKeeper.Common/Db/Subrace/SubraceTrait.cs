﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonKeeper.Common.Db.Subrace
{
    public class SubraceTrait
    {
        public int SubraceId { get; set; }
        public Subrace Subrace { get; set; }
        public int TraitId { get; set; }
        public Trait Trait { get; set; }
    }
}
