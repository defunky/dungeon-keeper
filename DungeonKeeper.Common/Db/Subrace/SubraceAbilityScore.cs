﻿namespace DungeonKeeper.Common.Db.Subrace
{
    public class SubraceAbilityScore
    {
        public int SubraceId { get; set; }
        public Subrace Subrace { get; set; }
        public int AbilityScoreId { get; set; }
        public AbilityScore AbilityScore { get; set; }
        public int Bonus { get; set; }
    }
}
