﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DungeonKeeper.Common.Db.Subrace
{
    public class Subrace
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int RaceId { get; set; }
        public SourceMaterial SourceMaterial { get; set; }

        public IList<SubraceAbilityScore> SubraceAbilityScores { get; set; }
        public IList<SubraceTrait> SubraceTraits { get; set; }
        public Race Race { get; set; }
        
    }
}
