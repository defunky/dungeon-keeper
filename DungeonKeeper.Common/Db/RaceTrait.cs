﻿namespace DungeonKeeper.Common.Db
{
    public class RaceTrait
    {
        public int TraitId { get; set; }
        public Trait Trait { get; set; }

        public int RaceId { get; set; }
        public Race Race { get; set; }
    }
}
