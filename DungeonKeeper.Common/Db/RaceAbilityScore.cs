﻿namespace DungeonKeeper.Common.Db
{
    public class RaceAbilityScore
    {
        public int RaceId { get; set; }
        public Race Race { get; set; }
        public int AbilityScoreId { get; set; }
        public AbilityScore AbilityScore { get; set; }
        public int Bonus { get; set; }
    }
}
