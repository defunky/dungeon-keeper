﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DungeonKeeper.Common.Db.Subrace;

namespace DungeonKeeper.Common.Db
{
    public class Trait
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<RaceTrait> RaceTraits { get; set; }
        public IList<SubraceTrait> SubraceTraits { get; set; }

    }
}
