﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using DungeonKeeper.Common.Db.Subrace;

namespace DungeonKeeper.Common.Db
{
    public class AbilityScore
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public string Check { get; set; }

        //TODO: These probably aren't needed so maybe remove
        [IgnoreDataMember]
        public IList<RaceAbilityScore> RaceAbilityScores { get; set; }
        [IgnoreDataMember]
        public IList<SubraceAbilityScore> SubraceAbilityScores { get; set; }
        public IList<Skill> Skills { get; set; }
    }
}
