﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DungeonKeeper.Common.Db
{
    public class Language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Script { get; set; }
        public string Type { get; set; }
        public SourceMaterial SourceMaterial { get; set; }
        public IList<RaceLanguage> RaceLanguages { get; set; }
    }
}
