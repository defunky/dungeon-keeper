﻿using System.ComponentModel;

namespace DungeonKeeper.Common.Db
{
    public enum SourceMaterial
    {
        [Description("Player's Handbook")]
        PHB,
        [Description("Monsters Manual")]
        MM,
        [Description("Mordenkainen's Tome of Foes")]
        MToF,
        [Description("Swords Coast Adventurer's Guide")]
        SCAG,
        [Description("Volo's Guide to Monsters")]
        VGtM,
        [Description("Guildmasters' Guide to Ravnica")]
        GGtR,
        [Description("Wayfinder's Guide to Eberron")]
        WGtE,
        [Description("Dungeon Master's Guide")]
        DMG
    }
}
