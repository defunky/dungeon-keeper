﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace DungeonKeeper.API.Services.Interfaces
{
    public interface IJwtTokenService
    {
        string BuildToken(IdentityUser user, IEnumerable<string> roles);
        (string Username, string Email) GetTokenDetails(string token);
        string GetHeaderToken(IHeaderDictionary headers);
    }
}
