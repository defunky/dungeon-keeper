﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DungeonKeeper.Common;
using DungeonKeeper.Common.Db;
using DungeonKeeper.Common.Db.Subrace;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DungeonKeeper.API.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Trait> Traits { get; set; }
        public DbSet<RaceTrait> RaceTraits { get; set; }
        public DbSet<SubraceTrait> SubraceTraits { get; set; }

        public DbSet<Race> Races { get; set; }
        public DbSet<Subrace> Subraces { get; set; }

        public DbSet<RaceLanguage> RaceLanguages { get; set; }
        public DbSet<Language> Languages { get; set; }

        public DbSet<AbilityScore> AbilityScores { get; set; }
        public DbSet<RaceAbilityScore> RaceAbilityScores { get; set; }
        public DbSet<SubraceAbilityScore> SubraceAbilityScores { get; set; }

        public DbSet<Skill> Skills { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RaceTrait>().HasKey(x => new {x.RaceId, x.TraitId}); 
            builder.Entity<SubraceTrait>().HasKey(x => new { x.SubraceId, x.TraitId });
            builder.Entity<RaceLanguage>().HasKey(x => new {x.RaceId, x.LanguageId});
            builder.Entity<RaceAbilityScore>().HasKey(x => new { x.RaceId, x.AbilityScoreId });
            builder.Entity<SubraceAbilityScore>().HasKey(x => new { x.SubraceId, x.AbilityScoreId });

            base.OnModelCreating(builder);
        }
    }
}
