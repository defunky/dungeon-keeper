﻿using DungeonKeeper.API.Data;
using DungeonKeeper.Common;
using DungeonKeeper.Common.Db;
using DungeonKeeper.Common.Db.Subrace;
using Microsoft.AspNetCore.Mvc;

namespace DungeonKeeper.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SeedController : ControllerBase
    {
        private readonly ApplicationDbContext m_context;

        public SeedController(ApplicationDbContext context)
        {
            m_context = context;
        }

        [HttpGet]
        [Route("AbilityScore")]
        public IActionResult AbilityScore()
        {
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Strength",
                Abbreviation = "STR",
                Description = "Strength measures bodily power, athletic Training, and the extent to which you can exert raw physical force.",
                Check = "A Strength check can model any attempt to lift, push, pull, or break something, to force your body through a space, or to otherwise apply brute force to a situation. " +
                         "The Athletics skill reflects aptitude in certain kinds of Strength checks."

            });
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Dexterity",
                Abbreviation = "DEX",
                Description = "Dexterity measures agility, reflexes, and balance.",
                Check = "A Dexterity check can model any attempt to move nimbly, quickly, or quietly, or to keep from falling on tricky footing. " +
                        "The Acrobatics, Sleight of Hand, and Stealth skills reflect aptitude in certain kinds of Dexterity checks. "
            });
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Constitution",
                Abbreviation = "CON",
                Description = "Constitution measures health, stamina, and vital force. ",
                Check = "Constitution checks are uncommon, and no skills apply to Constitution checks, because the endurance this ability represents is largely passive rather than involving a specific effort on the part of a character or monster."
            });
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Intelligence",
                Abbreviation = "INT",
                Description = "Intelligence measures mental acuity, accuracy of recall, and the ability to reason. ",
                Check = "An Intelligence check comes into play when you need to draw on logic, education, memory, or deductive reasoning. " +
                        "The Arcana, History, Investigation, Nature, and Religion skills reflect aptitude in certain kinds of Intelligence checks."
            });
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Wisdom",
                Abbreviation = "WIS",
                Description = "Wisdom reflects how attuned you are to the world around you and represents perceptiveness and intuition. ",
                Check = "A Wisdom check might reflect an effort to read body language, understand someone's feelings, notice things about the environment, or care for an injured person. " +
                        "The Animal Handling, Insight, Medicine, Perception, and Survival skills reflect aptitude in certain kinds of Wisdom checks."
            });
            m_context.AbilityScores.Add(new AbilityScore
            {
                Name = "Charisma",
                Abbreviation = "CHA",
                Description = "Charisma measures your ability to interact effectively with others. It includes such factors as confidence and eloquence, and it can represent a charming or commanding personality.",
                Check = "A Charisma check might arise when you try to influence or entertain others, when you try to make an impression or tell a convincing lie, or when you are navigating a tricky social situation. " +
                        "The Deception, Intimidation, Performance, and Persuasion skills reflect aptitude in certain kinds of Charisma checks."
            });

            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("Skill")]
        public IActionResult Skill()
        {
            m_context.Skills.Add(new Skill
            {
                Name = "Acrobatics",
                Description = "Your Dexterity (Acrobatics) check covers your attempt to stay on your feet in a tricky situation, " +
                              "such as when you're trying to run across a sheet of ice, balance on a tightrope, or stay upright on a rocking ship's deck." +
                              " The GM might also call for a Dexterity (Acrobatics) check to see if you can perform acrobatic stunts, including dives, rolls, somersaults, and flips.",
                AbilityScoreId = 2
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Animal Handling",
                Description = "When there is any question whether you can calm down a domesticated animal, keep a mount from getting spooked, or intuit an animal's intentions, the GM might call for a Wisdom (Animal Handling) check. You also make a Wisdom (Animal Handling) check to control your mount when you attempt a risky maneuver.",
                AbilityScoreId = 5
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Arcana",
                Description = "Your Intelligence (Arcana) check measures your ability to recall lore about spells, magic items, eldritch symbols, magical traditions, the planes of existence, and the inhabitants of those planes.",
                AbilityScoreId = 4
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Athletics",
                Description = "Your Strength (Athletics) check covers difficult situations you encounter while climbing, jumping, or swimming.",
                AbilityScoreId = 1
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Deception",
                Description = "Your Charisma (Deception) check determines whether you can convincingly hide the truth, either verbally or through your actions. This deception can encompass everything from misleading others through ambiguity to telling outright lies. Typical situations include trying to fast- talk a guard, con a merchant, earn money through gambling, pass yourself off in a disguise, dull someone's suspicions with false assurances, or maintain a straight face while telling a blatant lie.",
                AbilityScoreId = 6
            });
            m_context.Skills.Add(new Skill
            {
                Name = "History",
                Description = "Your Intelligence (History) check measures your ability to recall lore about historical events, legendary people, ancient kingdoms, past disputes, recent wars, and lost civilizations.",
                AbilityScoreId = 4
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Insight",
                Description = "Your Wisdom (Insight) check decides whether you can determine the true intentions of a creature, such as when searching out a lie or predicting someone's next move. Doing so involves gleaning clues from body language, speech habits, and changes in mannerisms.",
                AbilityScoreId = 5
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Intimidation",
                Description = "When you attempt to influence someone through overt threats, hostile actions, and physical violence, the GM might ask you to make a Charisma (Intimidation) check. Examples include trying to pry information out of a prisoner, convincing street thugs to back down from a confrontation, or using the edge of a broken bottle to convince a sneering vizier to reconsider a decision.",
                AbilityScoreId = 6
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Investigation",
                Description = "When you look around for clues and make deductions based on those clues, you make an Intelligence (Investigation) check. You might deduce the location of a hidden object, discern from the appearance of a wound what kind of weapon dealt it, or determine the weakest point in a tunnel that could cause it to collapse. Poring through ancient scrolls in search of a hidden fragment of knowledge might also call for an Intelligence (Investigation) check.",
                AbilityScoreId = 4
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Medicine",
                Description = "A Wisdom (Medicine) check lets you try to stabilize a dying companion or diagnose an illness.",
                AbilityScoreId = 5
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Nature",
                Description = "Your Intelligence (Nature) check measures your ability to recall lore about terrain, plants and animals, the weather, and natural cycles.",
                AbilityScoreId = 4
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Perception",
                Description = "Your Wisdom (Perception) check lets you spot, hear, or otherwise detect the presence of something. It measures your general awareness of your surroundings and the keenness of your senses. For example, you might try to hear a conversation through a closed door, eavesdrop under an open window, or hear monsters moving stealthily in the forest. Or you might try to spot things that are obscured or easy to miss, whether they are orcs lying in ambush on a road, thugs hiding in the shadows of an alley, or candlelight under a closed secret door.",
                AbilityScoreId = 5
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Performance",
                Description = "Your Charisma (Performance) check determines how well you can delight an audience with music, dance, acting, storytelling, or some other form of entertainment.",
                AbilityScoreId = 6
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Persuasion",
                Description = "When you attempt to influence someone or a group of people with tact, social graces, or good nature, the GM might ask you to make a Charisma (Persuasion) check. Typically, you use persuasion when acting in good faith, to foster friendships, make cordial requests, or exhibit proper etiquette. Examples of persuading others include convincing a chamberlain to let your party see the king, negotiating peace between warring tribes, or inspiring a crowd of townsfolk.",
                AbilityScoreId = 6
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Religion",
                Description = "Your Intelligence (Religion) check measures your ability to recall lore about deities, rites and prayers, religious hierarchies, holy symbols, and the practices of secret cults.",
                AbilityScoreId = 4
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Sleight of Hand",
                Description = "Whenever you attempt an act of legerdemain or manual trickery, such as planting something on someone else or concealing an object on your person, make a Dexterity (Sleight of Hand) check. The GM might also call for a Dexterity (Sleight of Hand) check to determine whether you can lift a coin purse off another person or slip something out of another person's pocket.",
                AbilityScoreId = 2
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Stealth",
                Description = "Make a Dexterity (Stealth) check when you attempt to conceal yourself from enemies, slink past guards, slip away without being noticed, or sneak up on someone without being seen or heard.",
                AbilityScoreId = 2
            });
            m_context.Skills.Add(new Skill
            {
                Name = "Survival",
                Description = "The GM might ask you to make a Wisdom (Survival) check to follow tracks, hunt wild game, guide your group through frozen wastelands, identify signs that owlbears live nearby, predict the weather, or avoid quicksand and other natural hazards.",
                AbilityScoreId = 5
            });

            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("Language")]
        public IActionResult Language()
        {
            m_context.Languages.Add(new Language
            {
                Name = "Common",
                Script = "Common",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Dwarvish",
                Script = "Dwarvish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Elvish",
                Script = "Evlish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Giant",
                Script = "Dwarvish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Gnomish",
                Script = "Dwarvish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Goblin",
                Script = "Dwarvish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Halfling",
                Script = "Common",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Orc",
                Script = "Dwarvish",
                Type = "Standard",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Abyssal",
                Script = "Abyssal",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Celestial",
                Script = "Common",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Draconic",
                Script = "Draconic",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Deep-Speech",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Infernal",
                Script = "Infernal",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Primordial",
                Script = "Dwarvish",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Sylvan",
                Script = "Elvish",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Languages.Add(new Language
            {
                Name = "Undercommon",
                Script = "Elvish",
                Type = "Exotic",
                SourceMaterial = SourceMaterial.PHB
            });

            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("Trait")]
        public IActionResult Trait()
        {
            m_context.Traits.Add(new Trait
            {
                Name = "Draconic Ancestry",
                Description = "You have draconic ancestry. Choose one type of dragon from the Draconic Ancestry table. Your breath weapon and damage resistance are determined by the dragon type, as shown in the table."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Breath Weapon",
                Description = "You can use your action to exhale destructive energy. Your draconic ancestry determines the size, shape, and damage type of the exhalation. When you use your breath weapon, each creature in the area of the exhalation must make a saving throw, the type of which is determined by your draconic ancestry. The DC for this saving throw equals 8 + your Constitution modifier + your proficiency bonus. A creature takes 2d6 damage on a failed save, and half as much damage on a successful one. The damage increases to 3d6 at 6th level, 4d6 at 11th level, and 5d6 at 16th level. After you use your breath weapon, you can’t use it again until you complete a short or long rest."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Damage Resistance",
                Description = "You have resistance to the damage type associated with your draconic ancestry."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Darkvision",
                Description = "You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Dwarven Resilience",
                Description = "You have advantage on saving throws against poison, and you have resistance against poison damage."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Dwarven Combat Training",
                Description = "You have proficiency with the battleaxe, handaxe, light hammer, and warhammer."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Tool Proficiency",
                Description = "You gain proficiency with the artisan’s tools of your choice: smith’s tools, brewer’s supplies, or mason’s tools."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Stonecunning",
                Description = "Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Dwarven Armor Training",
                Description = "You have proficiency with light and medium armor."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Keen Senses",
                Description = "You have proficiency in the Perception skill."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Fey Ancestry",
                Description = "You have advantage on saving throws against being charmed, and magic can’t put you to sleep."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Trance",
                Description = "Elves don’t need to sleep. Instead, they meditate deeply, remaining semiconscious, for 4 hours a day. (The Common word for such meditation is “trance.”) While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Elf Weapon Training",
                Description = "You have proficiency with the longsword, shortsword, shortbow, and longbow."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Cantrip",
                Description = "You know one cantrip of your choice from the wizard spell list. Intelligence is your spellcasting ability for it."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Extra Language",
                Description = "You can speak, read, and write one extra language of your choice."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Sunlight Sensitivity",
                Description = "You have disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight when you, the target of your attack, or whatever you are trying to perceive is in direct sunlight."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Drow Magic",
                Description = "You know the dancing lights cantrip. When you reach 3rd level, you can cast the faerie fire spell once per day. When you reach 5th level, you can also cast the darkness spell once per day. Charisma is your spellcasting ability for these spells."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Drow Weapon Training",
                Description = "You have proficiency with rapiers, shortswords, and hand crossbows."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Fleet of Foot",
                Description = "Your base walking speed increases to 35 feet."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Mask of the Wild",
                Description = "You can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Gnome Cunning",
                Description = "You have advantage on all Intelligence, Wisdom, and Charisma saving throws against magic."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Superior Darkvision",
                Description = "Your darkvision has a radius of 120 feet."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Stone Camouflage",
                Description = "You have advantage on Dexterity (stealth) checks to hide in rocky terrain."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Artificer’s Lore",
                Description = "Whenever you make an Intelligence (History) check related to magic items, alchemical objects, or technological devices, you can add twice your proficiency bonus, instead of any proficiency bonus you normally apply."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Tinker",
                Description = "You have proficiency with artisan’s tools (tinker’s tools). Using those tools, you can spend 1 hour and 10 gp worth of materials to construct a Tiny clockwork device (AC 5, 1 hp). The device ceases to function after 24 hours (unless you spend 1 hour repairing it to keep the device functioning), or when you use your action to dismantle it; at that time, you can reclaim the materials used to create it. You can have up to three such devices active at a time."
                                + "When you create a device, choose one of the following options:"
                                + "Clockwork Toy. This toy is a clockwork animal, monster, or person, such as a frog, mouse, bird, dragon, or soldier. When placed on the ground, the toy moves 5 feet across the ground on each of your turns in a random direction. It makes noises as appropriate to the creature it represents."
                                + "Fire Starter. The device produces a miniature flame, which you can use to light a candle, torch, or campfire. Using the device requires your action."
                                + "Music Box. When opened, this music box plays a single song at a moderate volume."
                                + "The box stops playing when it reaches the song’s end or when it is closed."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Skill Versatility",
                Description = "You gain proficiency in two skills of your choice."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Menacing",
                Description = "You gain proficiency in the Intimidation skill."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Relentless Endurance",
                Description = "When you are reduced to 0 hit points but not killed outright, you can drop to 1 hit point instead. You can’t use this feature again until you finish a long rest."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Savage Attacks",
                Description = "When you score a critical hit with a melee weapon attack, you can roll one of the weapon’s damage dice one additional time and add it to the extra damage of the critical hit."
            }); m_context.Traits.Add(new Trait
            {
                Name = "Lucky",
                Description = "When you roll a 1 on the d20 for an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Brave",
                Description = "You have advantage on saving throws against being frightened."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Halfling Nimbleness",
                Description = "You can move through the space of any creature that is of a size larger than yours."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Naturally Stealthy",
                Description = "You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Stout Resilience",
                Description = "You have advantage on saving throws against poison, and you have resistance against poison damage."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Skills",
                Description = "You gain proficiency in one skill of your choice."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Feat",
                Description = "You gain one feat of your choice."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Hellish Resistance",
                Description = "You have resistance to fire damage."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Infernal Legacy",
                Description = "You know the thaumaturgy cantrip. When you reach 3rd level, you can cast the hellish rebuke spell as a 2nd-level spell once with this trait and regain the ability to do so when you finish a long rest. When you reach 5th level, you can cast the darkness spell once with this trait and regain the ability to do so when you finish a long rest. Charisma is your spellcasting ability for these spells."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Natural Illusionist",
                Description = "You know the minor illusion cantrip. Intelligence is your spellcasting ability for it."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Speak with Small Beasts",
                Description = "Through sounds and gestures,you can communicate simple ideas with Small or smaller beasts.Forest gnomes love animals and often keep squirrels, badgers, rabbits, moles, woodpeckers, and other creatures as beloved pets."
            });
            m_context.Traits.Add(new Trait
            {
                Name = "Dwarven Toughness",
                Description = "Your hit point maximum increases by 1, and it increases by 1 every time you gain a level."
            });
            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("Race")]
        public IActionResult Race()
        {
            m_context.Races.Add(new Race
            {
                Name = "Dragonborn",
                Description = "Dragonborn look very much like dragons standing erect in humanoid form, though they lack wings or a tail.",
                Age = "Young dragonborn grow quickly. They walk hours after hatching, attain the size and development of a 10-year-old human child by the age of 3, and reach adulthood by 15. They live to be around 80.",
                Alignment = "Dragonborn tend to extremes, making a conscious choice for one side or the other in the cosmic war between good and evil (represented by Bahamut and Tiamat, respectively). Most dragonborn are good, but those who side with Tiamat can be terrible villains.",
                Size = Sizes.Medium,
                SizeDescription = "Dragonborn are taller and heavier than humans, standing well over 6 feet tall and averaging almost 250 pounds. Your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common and Draconic. Draconic is thought to be one of the oldest languages and is often used in the study of magic. The language sounds harsh to most other creatures and includes numerous hard consonants and sibilants.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Dwarf",
                Description = "Bold and hardy, dwarves are known as skilled warriors, miners, and workers of stone and metal.",
                Age = "Dwarves mature at the same rate as humans, but they’re considered young until they reach the age of 50. On average, they live about 350 years.",
                Alignment = "Most dwarves are lawful, believing firmly in the benefits of a well-ordered society. They tend toward good as well, with a strong sense of fair play and a belief that everyone deserves to share in the benefits of a just order.",
                Size = Sizes.Medium,
                SizeDescription = "Dwarves stand between 4 and 5 feet tall and average about 150 pounds. Your size is Medium.",
                Speed = 25,
                SpeedDescription = "Your base walking speed is 25 feet. Your speed is not reduced by wearing heavy armor.",
                LanguageDescription = "You can speak, read, and write Common and Dwarvish. Dwarvish is full of hard consonants and guttural sounds, and those characteristics spill over into whatever other language a dwarf might speak.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Elf",
                Description = "Elves are a magical people of otherworldly grace, living in the world but not entirely part of it.",
                Age = "Although elves reach physical maturity at about the same age as humans, the elven understanding of adulthood goes beyond physical growth to encompass worldly experience. An elf typically claims adulthood and an adult name around the age of 100 and can live to be 750 years old.",
                Alignment = "Elves love freedom, variety, and self-expression, so they lean strongly toward the gentler aspects of chaos. They value and protect others’ freedom as well as their own, and they are more often good than not. The drow are an exception; their exile into the Underdark has made them vicious and dangerous. Drow are more often evil than not.",
                Size = Sizes.Medium,
                SizeDescription = "Elves range from under 5 to over 6 feet tall and have slender builds. Your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common and Elvish. Elvish is fluid, with subtle intonations and intricate grammar. Elven literature is rich and varied, and their songs and poems are famous among other races. Many bards learn their language so they can add Elvish ballads to their repertoires.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Gnome",
                Description = "A gnome’s energy and enthusiasm for living shines through every inch of his or her tiny body.",
                Age = "Gnomes mature at the same rate humans do, and most are expected to settle down into an adult life by around age 40. They can live 350 to almost 500 years.",
                Alignment = "Gnomes are most often good. Those who tend toward law are sages, engineers, researchers, scholars, investigators, or inventors. Those who tend toward chaos are minstrels, tricksters, wanderers, or fanciful jewelers. Gnomes are good-hearted, and even the tricksters among them are more playful than vicious.",
                Size = Sizes.Small,
                SizeDescription = "Gnomes are between 3 and 4 feet tall and average about 40 pounds. Your size is Small.",
                Speed = 25,
                SpeedDescription = "Your base walking speed is 25 feet.",
                LanguageDescription = "You can speak, read, and write Common and Gnomish. The Gnomish language, which uses the Dwarvish script, is renowned for its technical treatises and its catalogs of knowledge about the natural world.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Half-Elf",
                Description = "Half-elves combine what some say are the best qualities of their elf and human parents.",
                Age = "Half-elves mature at the same rate humans do and reach adulthood around the age of 20. They live much longer than humans, however, often exceeding 180 years",
                Alignment = "Half-elves share the chaotic bent of their elven heritage. They value both personal freedom and creative expression, demonstrating neither love of leaders nor desire for followers. They chafe at rules, resent others’ demands, and sometimes prove unreliable, or at least unpredictable.",
                Size = Sizes.Medium,
                SizeDescription = "Half-elves are about the same size as humans, ranging from 5 to 6 feet tall. Your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common, Elvish, and one extra language of your choice.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Half-Orc",
                Description = "Half-orcs’ grayish pigmentation, sloping foreheads, jutting jaws, prominent teeth, and towering builds make their orcish heritage plain for all to see.",
                Age = "Half-orcs mature a little faster than humans, reaching adulthood around age 14. They age noticeably faster and rarely live longer than 75 years.",
                Alignment = "Half-orcs inherit a tendency toward chaos from their orc parents and are not strongly inclined toward good. Half-orcs raised among orcs and willing to live out their lives among them are usually evil.",
                Size = Sizes.Medium,
                SizeDescription = "Half-orcs are somewhat larger and bulkier than humans, and they range from 5 to well over 6 feet tall. Your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common and Orc. Orc is a harsh, grating language with hard consonants. It has no script of its own but is written in the Dwarvish script.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Halfling",
                Description = "The diminutive halflings survive in a world full of larger creatures by avoiding notice or, barring that, avoiding offense.",
                Age = "A halfling reaches adulthood at the age of 20 and generally lives into the middle of his or her second century.",
                Alignment = "Most halflings are lawful good. As a rule, they are good-hearted and kind, hate to see others in pain, and have no tolerance for oppression. They are also very orderly and traditional, leaning heavily on the support of their community and the comfort of their old ways.",
                Size = Sizes.Small,
                SizeDescription = "Halflings average about 3 feet tall and weigh about 40 pounds. Your size is Small.",
                Speed = 25,
                SpeedDescription = "Your base walking speed is 25 feet.",
                LanguageDescription = "You can speak, read, and write Common and Halfling. The Halfling language isn’t secret, but halflings are loath to share it with others. They write very little, so they don’t have a rich body of literature. Their oral tradition, however, is very strong. Almost all halflings speak Common to converse with the people in whose lands they dwell or through which they are traveling.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Human",
                Description = "Humans are the most adaptable and ambitious people among the common races. Whatever drives them, humans are the innovators, the achievers, and the pioneers of the worlds.",
                Age = "Humans reach adulthood in their late teens and live less than a century.",
                Alignment = "Humans tend toward no particular alignment. The best and the worst are found among them.",
                Size = Sizes.Medium,
                SizeDescription = "Humans vary widely in height and build, from barely 5 feet to well over 6 feet tall. Regardless of your position in that range, your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common and one extra language of your choice. Humans typically learn the languages of other peoples they deal with, including obscure dialects. They are fond of sprinkling their speech with words borrowed from other tongues: Orc curses, Elvish musical expressions, Dwarvish military phrases, and so on.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Races.Add(new Race
            {
                Name = "Tiefling",
                Description = "To be greeted with stares and whispers, to suffer violence and insult on the street, to see mistrust and fear in every eye: this is the lot of the tiefling.",
                Age = "Tieflings mature at the same rate as humans but live a few years longer.",
                Alignment = "Tieflings might not have an innate tendency toward evil, but many of them end up there. Evil or not, an independent nature inclines many tieflings toward a chaotic alignment.",
                Size = Sizes.Medium,
                SizeDescription = "Tieflings are about the same size and build as humans. Your size is Medium.",
                Speed = 30,
                SpeedDescription = "Your base walking speed is 30 feet.",
                LanguageDescription = "You can speak, read, and write Common and Infernal.",
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("RaceLanguage")]
        public IActionResult RaceLanguage()
        {
            //TODO: Need to do choice related ones like humans trait
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 1,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 1,
                LanguageId = 11
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 2,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 2,
                LanguageId = 2
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 3,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 3,
                LanguageId = 3
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 4,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 4,
                LanguageId = 5
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 4,
                LanguageId = 16
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 5,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 5,
                LanguageId = 3
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 6,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 6,
                LanguageId = 8
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 7,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 7,
                LanguageId = 7
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 8,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 9,
                LanguageId = 1
            });
            m_context.RaceLanguages.Add(new RaceLanguage
            {
                RaceId = 9,
                LanguageId = 13
            });

            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("RaceAbilityScore")]
        public IActionResult RaceAbilityScore()
        {
            //TODO: Need to add choice bonuses like half-elf one
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 1,
                AbilityScoreId = 1,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 1,
                AbilityScoreId = 6,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 2,
                AbilityScoreId = 3,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 3,
                AbilityScoreId = 2,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 4,
                AbilityScoreId = 4,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 5,
                AbilityScoreId = 6,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 6,
                AbilityScoreId = 1,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 6,
                AbilityScoreId = 3,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 7,
                AbilityScoreId = 2,
                Bonus = 2
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 1,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 2,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 3,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 4,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 5,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 8,
                AbilityScoreId = 6,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 9,
                AbilityScoreId = 4,
                Bonus = 1
            });
            m_context.RaceAbilityScores.Add(new RaceAbilityScore
            {
                RaceId = 9,
                AbilityScoreId = 6,
                Bonus = 2
            });
            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("RaceTrait")]
        public IActionResult RaceTrait()
        {
            //TODO: Half elf AS choice

            //Dragonborn
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 1,
                TraitId = 1
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 1,
                TraitId = 21
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 1,
                TraitId = 22
            });

            //Dwarf
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 2,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 2,
                TraitId = 24
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 2,
                TraitId = 25
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 2,
                TraitId = 26
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 2,
                TraitId = 27
            });

            //Elf
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 3,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 3,
                TraitId = 37
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 3,
                TraitId = 30
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 3,
                TraitId = 31
            });

            //Gnome
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 4,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 4,
                TraitId = 17
            });

            //Half-Elf
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 5,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 5,
                TraitId = 30
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 5,
                TraitId = 6
            });

            //Half-Orc
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 6,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 6,
                TraitId = 7
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 6,
                TraitId = 8
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 6,
                TraitId = 9
            });

            //Halfling
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 7,
                TraitId = 10
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 7,
                TraitId = 11
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 7,
                TraitId = 12
            });

            //Tiefling
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 9,
                TraitId = 23
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 9,
                TraitId = 28
            });
            m_context.RaceTraits.Add(new RaceTrait
            {
                RaceId = 9,
                TraitId = 38
            });

            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("Subrace")]
        public IActionResult Subrace()
        {
            m_context.Subraces.Add(new Subrace
            {
                Name = "Hill Dwarf",
                RaceId = 2,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Mountain Dwarf",
                RaceId = 2,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "High Elf",
                RaceId = 3,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Wood Elf",
                RaceId = 3,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Dark Elf (Drow)",
                RaceId = 3,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Forest Gnome",
                RaceId = 4,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Rock Gnome",
                RaceId = 4,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Lightfoot Halfling",
                RaceId = 7,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Stout Halfling",
                RaceId = 7,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Human",
                RaceId = 8,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.Subraces.Add(new Subrace
            {
                Name = "Variant Human",
                RaceId = 8,
                SourceMaterial = SourceMaterial.PHB
            });
            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("SubraceAS")]
        public IActionResult SubraceAs()
        {
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 1,
                AbilityScoreId = 5,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 2,
                AbilityScoreId = 1,
                Bonus = 2
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 3,
                AbilityScoreId = 4,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 4,
                AbilityScoreId = 5,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 5,
                AbilityScoreId = 6,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 6,
                AbilityScoreId = 2,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 7,
                AbilityScoreId = 3,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 8,
                AbilityScoreId = 6,
                Bonus = 1
            });
            m_context.SubraceAbilityScores.Add(new SubraceAbilityScore
            {
                SubraceId = 9,
                AbilityScoreId = 3,
                Bonus = 1
            });
            m_context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        [Route("SubraceTraits")]
        public IActionResult SubraceTraits()
        {
            //Hill D
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 1,
                TraitId = 41
            });

            //Mountain D
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 2,
                TraitId = 29
            });

            //High Elf
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 3,
                TraitId = 23
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 3,
                TraitId = 33
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 3,
                TraitId = 34
            });

            //Wood Elf
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 4,
                TraitId = 32
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 4,
                TraitId = 19
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 4,
                TraitId = 18
            });

            //Drow
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 5,
                TraitId = 2
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 5,
                TraitId = 35
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 5,
                TraitId = 36
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 5,
                TraitId = 20
            });

            //Forest Gnome
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 6,
                TraitId = 39
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 6,
                TraitId = 40
            });

            //Rock Gnome
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 7,
                TraitId = 4
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 7,
                TraitId = 5
            });

            //Lightfoot Gnome
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 8,
                TraitId = 13
            });

            //Stout
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 9,
                TraitId = 14
            });

            //Variant Human
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 11,
                TraitId = 15
            });
            m_context.SubraceTraits.Add(new SubraceTrait()
            {
                SubraceId = 11,
                TraitId = 16
            });

            m_context.SaveChanges();
            return Ok();
        }
    }
}