﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DungeonKeeper.API.Services.Interfaces;
using DungeonKeeper.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DungeonKeeper.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> m_log;
        private readonly UserManager<IdentityUser> m_userManager;
        private readonly RoleManager<IdentityRole> m_roleManager;
        private readonly IJwtTokenService m_tokenService;

        public UserController(ILogger<UserController> log,
            UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, IJwtTokenService tokenService)
        {
            m_log = log;
            m_userManager = userManager;
            m_roleManager = roleManager;
            m_tokenService = tokenService;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterUser user)
        {
            if (!ModelState.IsValid) return BadRequest();

            var newUser = await m_userManager.CreateAsync(new IdentityUser
            {
                UserName = user.Username,
                Email = user.Email,
            }, user.Password);

            if (!newUser.Succeeded)
            {
                m_log.Log(LogLevel.Debug, $"Failed to create a new user, Error: {newUser.Errors}");
                return BadRequest(newUser.Errors);
            }

            return Ok();
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] User user)
        {
            var identityUser = await m_userManager.FindByNameAsync(user.Username);
            if (identityUser == null) return Unauthorized();

            var success = await m_userManager.CheckPasswordAsync(identityUser, user.Password);
            if (!success)
            {
                m_log.Log(LogLevel.Debug, $"Failed to login user: ${identityUser.UserName}");
                return Unauthorized();
            }

            return Ok(new { token = m_tokenService.BuildToken(identityUser, await m_userManager.GetRolesAsync(identityUser)) });
        }
    }
}