﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DungeonKeeper.API.Data;
using DungeonKeeper.Common;
using DungeonKeeper.Common.Db;
using DungeonKeeper.Common.Db.Subrace;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DungeonKeeper.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RacesController : ControllerBase
    {
        private readonly ApplicationDbContext m_context;

        public RacesController(ApplicationDbContext context)
        {
            m_context = context;
        }

        [HttpGet]
        [Route("Skills")]
        public IList<Skill> Skills()
        {
            return m_context.Skills.Include(x => x.AbilityScore).ToList();
        }

        [HttpGet]
        [Route("Traits/{name}")]
        public IList<Trait> Traits(string name)
        {
            return m_context.Traits.Where(x => x.Name == name).Include(x => x.RaceTraits).ToList();
        }

        [HttpGet]
        [Route("Subraces")]
        public IList<Subrace> Subraces()
        {
           return m_context.Subraces.Include(x => x.Race).Include(x => x.SubraceAbilityScores).ThenInclude(x=> x.AbilityScore).ToList();
        }

        [HttpGet]
        public IList<Race> Get()
        {
            return m_context.Races
                .Include(x => x.RaceLanguages).ThenInclude(x => x.Language)
                .Include(x=> x.RaceAbilityScores).ThenInclude(x => x.AbilityScore)
                .Include(x => x.RaceTraits).ThenInclude(x=> x.Trait).ThenInclude(x => x.RaceTraits)
                .Include(x => x.SubRaces)
                .ThenInclude(x => x.SubraceTraits).ThenInclude(x => x.Trait)
                .Include(x => x.SubRaces).ThenInclude(x => x.SubraceAbilityScores).ThenInclude(x => x.AbilityScore)
                .ToList();
        }
    }
}