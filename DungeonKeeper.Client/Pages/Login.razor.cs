﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using DungeonKeeper.Client.Data.Interfaces;
using DungeonKeeper.Common;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DungeonKeeper.Client.Pages
{
    public class LoginBase : ComponentBase
    {
        protected User m_user = new User();
        protected string m_passwordType = "password";
        protected string m_passwordIcon = "fa-eye";

        [Inject] private IAccountService AccountService { get; set; }
        [Inject] private ILocalStorageService StorageService { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }

        protected async Task userLogin()
        {
            var login = await AccountService.Login(m_user).ConfigureAwait(false);

            if (login.Success)
            {
                var token = (string)JsonConvert.DeserializeObject<JObject>(login.Content)["token"];
                await StorageService.SetItemAsync("token", token).ConfigureAwait(false);
                NavigationManager.NavigateTo("/", true);
            }

            //Refresh this object as it's will contain previous details on submission
            m_user = new User();
        }

        protected void showPassword()
        {
            switch (m_passwordType)
            {
                case "text":
                    m_passwordType = "password";
                    m_passwordIcon = "fa-eye";
                    break;
                case "password":
                    m_passwordType = "text";
                    m_passwordIcon = "fa-eye-slash";
                    break; 
            }
        }

    }
}
