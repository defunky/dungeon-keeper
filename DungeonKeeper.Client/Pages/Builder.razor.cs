﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DungeonKeeper.Client.Data.Interfaces;
using DungeonKeeper.Common.Db;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;

namespace DungeonKeeper.Client.Pages
{
    public class BuilderBase : ComponentBase
    {
        [Inject] private IRaceService RaceService { get; set; }
        protected IList<Race> Races { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                //TODO API Page, DTO models and use automappers to prevent deep nested classes.
                var res = await RaceService.Races().ConfigureAwait(false);
                Races = JsonConvert.DeserializeObject<IList<Race>>(res.Content);
                await InvokeAsync(StateHasChanged);
            }

        }
    }
}
