﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using DungeonKeeper.Client.Data.Interfaces;

namespace DungeonKeeper.Client.Data
{
    public class RaceService : ApiService, IRaceService
    {
        public RaceService(IHttpClientFactory factory, ILocalStorageService localStorage) : base(factory, localStorage)
        {
        }

        public async Task<(bool Success, string Content)> Races()
        {
            return await ExecuteGetAsync(new Uri(m_url + "/races"), false).ConfigureAwait(false);
        }
    }
}
