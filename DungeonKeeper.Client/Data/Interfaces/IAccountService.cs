﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DungeonKeeper.Common;

namespace DungeonKeeper.Client.Data.Interfaces
{
    public interface IAccountService
    {
        Task<(bool Success, string Content)> Register(RegisterUser user);
        Task<(bool Success, string Content)> Login(User user);
    }
}
