﻿using System.Threading.Tasks;

namespace DungeonKeeper.Client.Data.Interfaces
{
    public interface IRaceService
    {
        Task<(bool Success, string Content)> Races();
    }
}