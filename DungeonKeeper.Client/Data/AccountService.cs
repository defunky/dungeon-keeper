﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using DungeonKeeper.Client.Data.Interfaces;
using DungeonKeeper.Common;
using Newtonsoft.Json;

namespace DungeonKeeper.Client.Data
{
    public class AccountService : ApiService, IAccountService
    {
        public AccountService(IHttpClientFactory factory, ILocalStorageService localStorage) : base(factory, localStorage)
        {
        }

        public async Task<(bool Success, string Content)> Register(RegisterUser user)
        {
            return await ExecutePostAsync(new Uri(m_url + "/user/register"), JsonConvert.SerializeObject(user), false).ConfigureAwait(false);
        }

        public async Task<(bool Success, string Content)> Login(User user)
        {
            return await ExecutePostAsync(new Uri(m_url + "/user/login"), JsonConvert.SerializeObject(user), false).ConfigureAwait(false);
        }
    }
}
